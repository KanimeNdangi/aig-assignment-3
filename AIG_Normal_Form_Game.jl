### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ ee3a001d-d7d9-45f4-897a-bc55aedac55a
using PlutoUI

# ╔═╡ 82767f90-ddc2-11eb-090d-851aa299afbb


# ╔═╡ 31af9cd1-5cb6-4ad0-adea-e48e328e8294
md"## Program that captures sequential game between players in normal form and computes pay off of players with mixed strategy"

# ╔═╡ 86075c96-5e3b-4f8b-bc10-b5e2e9a4fc40


# ╔═╡ 68fb6208-b9a3-49a7-8ef3-39abadc56cc9


# ╔═╡ 53353ddc-c878-4d24-a0e1-f95d98ba8624


# ╔═╡ ea2f9c6f-f7fc-4c27-9aea-6526d7ea215d
md"#### Player Datatype"

# ╔═╡ dce39d06-7b16-45a9-aa49-9e371bcc03f6
begin
	struct Player{N,T<:Real}
    payoff_array::Array{T,N}

    function Player{N,T}(payoff_array::Array{T,N}) where {N,T<:Real}
        if prod(size(payoff_array)) == 0
            throw(ArgumentError("every player must have at least one action"))
        end
        return new(payoff_array)
    end
end
	
	Player(payoff_array::Array{T,N}) where {T<:Real,N} = Player{N,T}(payoff_array)
	
	Player{N,T}(player::Player{N,S}) where {N,T,S} = Player(Array{T}(player.payoff_array))
	
	Base.convert(::Type{T}, player::Player) where {T<:Player} = player isa T ? player : T(player)# instance of new player conversion
	
	Player(::Type{T}, player::Player{N}) where {T<:Real,N} = Player{N,T}(player)

    Player(player::Player{N,T}) where {N,T} = Player{N,T}(player)

    num_actions(p::Player) = size(p.payoff_array, 1)
    num_opponents(::Player{N}) where {N} = N - 1
end

# ╔═╡ 438cddda-45e1-4b1a-828e-e7780b3c1c6b
md"### Pure and Mixed Strategy Evaluation  methods and constructors "

# ╔═╡ bac84107-4d68-4b5d-b178-f4cb6c531b3f
const PureAction = Integer

# ╔═╡ 53fccfd5-9bea-47e4-ac45-f99394e3edd7
MixedAction{T<:Real} = Vector{T}

# ╔═╡ 6f8804a1-d2b8-4c09-80ac-d581ede71cde
Action{T<:Real} = Union{PureAction,MixedAction{T}}

# ╔═╡ d4c2bf5b-60cc-4cb9-8143-3b7a333a67c0
PureActionProfile{N,T<:PureAction} = NTuple{N,T}

# ╔═╡ 28632595-ed94-43e7-8b7d-32595eed0e9b
MixedActionProfile{T<:Real,N} = NTuple{N,MixedAction{T}}

# ╔═╡ 3129bff0-f4fc-4d91-8ffe-484404e8289f
const ActionProfile = Union{PureActionProfile,MixedActionProfile}

# ╔═╡ 246fc523-4ea7-4241-ae88-fea763c69395
Base.summary(player::Player) =
    string(Base.dims2string(size(player.payoff_array)),
           " ",
           split(string(typeof(player)), ".")[end])

# ╔═╡ 4b6e7fc6-9ba4-460a-9b77-c40d651365eb
function Base.show(io::IO, player::Player)
    print(io, summary(player))
    println(io, ":")
    Base.print_array(io, player.payoff_array)
end

# ╔═╡ e094541b-c5a8-4027-b007-e9eb016d0ad8
md"### Player Turn Instantiation
 
Return a new player instance with actions that are specified by action deleted from action set of player specified"

# ╔═╡ ebea3dec-353a-4281-be79-4b6d4cdffc98
# payoff_vector

# To resolve definition ambiguity
function payoff_vector(player::Player, opponents_actions::Tuple{})
    throw(ArgumentError("input tuple must not be empty"))
end

# ╔═╡ 5af39e18-3590-439c-9efc-127df06b7819
md"### Vector Payoff 

Given a tuple of the opponents' mixed actions return a vector of payoff values for a Player in an N>2 player game, one for each own action"

# ╔═╡ 507a3894-7213-4f7d-a568-57a455977c66
function payoff_vector(player::Player{N,T1},
                       opponents_actions::MixedActionProfile{T2}) where {N,T1,T2}
    length(opponents_actions) != num_opponents(player) &&
        throw(ArgumentError(
            "length of opponents_actions must be $(num_opponents(player))"
        ))
    S = promote_type(T1, T2)
    payoffs::Array{S,N} = player.payoff_array
    for i in num_opponents(player):-1:1
        payoffs = _reduce_ith_opponent(payoffs, i, opponents_actions[i])
    end
    return vec(payoffs)
end

# ╔═╡ df4fe028-bc0e-45a8-8018-5679348f319d
md"### Player Payoff 

Return a vector of payoff values for a Player in a 2-player game, one for each own action, given the opponent's mixed action."

# ╔═╡ 2809d30d-9229-4551-960a-c30d716f0693
function payoff_vector(player::Player{2}, opponent_action::MixedAction)
    # player.num_opponents == 1
    return player.payoff_array * opponent_action
end

# ╔═╡ 318543c1-2e9f-40bd-8364-5ed9e4feeade
# Return a vector of payoff values for a Player in a trivial game with 1 player, one for each own action.
function payoff_vector(player::Player{1}, opponent_action::Nothing)
    return player.payoff_array
end

# ╔═╡ 74c6b565-8cd3-48f0-a197-241d177245c0
# _reduce_ith_opponent

# Given an N-d array `payoff_array` of shape (n_{1}, ..., n_{i+1}, 1, ..., 1),
# return the N-d array of payoffs of shape (n_{1}, ..., n_{i}, 1, 1, ..., 1)
# given by fixing the i-th opponent's pure or mixed action to be `action`.
for (S, ex_mat_action) in ((PureAction, :(A[:, action])),
                           (MixedAction, :(A * action)))
    @eval function _reduce_ith_opponent(payoff_array::Array{T,N},
                                        i::Int, action::$S) where {N,T}
        shape = size(payoff_array)
        A = reshape(payoff_array, (prod(shape[1:i]), shape[i+1]))::Matrix{T}
        out = $(ex_mat_action)
        shape_new = tuple(shape[1:i]..., ones(Int, N-i)...)::NTuple{N,Int}
        return reshape(out, shape_new)
    end
end

# ╔═╡ 4ef765ed-7e3b-4588-81f2-b0ced5ca0356
md"#### Mixed Strategy Function Definitions"

# ╔═╡ cd4df2be-8538-41a0-aad8-71bd9e477f9a
#is_best_response return true if `own_action` is a best response to `opponents_actionss`.
function is_best_response(player::Player,
                          own_action::MixedAction,
                          opponents_actions::Union{Action,ActionProfile,Nothing};
                          tol::Real=1e-8)
    payoffs = payoff_vector(player, opponents_actions)
    payoff_max = maximum(payoffs)
    return dot(own_action, payoffs) >= payoff_max - tol
end

# ╔═╡ 7e9c8d2f-2cba-4195-a7fc-4fe776930ccc


# ╔═╡ de06ae24-5f0c-496e-b4fc-84015cbdf926
# Return a best response action to `opponents_actions
function best_response(rng::AbstractRange, player::Player,
                       opponents_actions::Union{Action,ActionProfile,Nothing};
                       tie_breaking::Symbol=:smallest,
                       tol::Real=1e-8)
    if tie_breaking == :smallest
        payoffs = payoff_vector(player, opponents_actions)
        return argmax(payoffs)
    elseif tie_breaking == :random
        brs = best_responses(player, opponents_actions; tol=tol)
        return rand(rng, brs)
    else
        throw(ArgumentError(
            "tie_breaking must be one of `:smallest` or `:random`"
        ))
    end
end

# ╔═╡ 1333118f-e4b6-447f-9962-843a49aa2cea
best_response(player::Player,
              opponents_actions::Union{Action,ActionProfile,Nothing};
              tie_breaking::Symbol=:smallest, tol::Real=1e-8) =
    best_response(Random.GLOBAL_RNG, player, opponents_actions,
                  tie_breaking=tie_breaking, tol=tol)

# ╔═╡ 64962491-a1ad-4fac-9097-d5f4a56e1d05
md"### NormalFormGame Type representing an N-player normal form game."

# ╔═╡ a106f03c-2a93-406a-9190-724a18680e94
# Check that the shapes of the payoff arrays are consistent
function is_consistent(players::NTuple{N,Player{N,T}}) where {N,T}
    shape_1 = size(players[1].payoff_array)
    for i in 2:N
        shape = size(players[i].payoff_array)
        for j in 1:(N-i+1)
            shape[j] == shape_1[i-1+j] || return false
        end
        for j in (N-i+2):N
            shape[j] == shape_1[j-(N-i+1)] || return false
        end
    end
    return true
	
	
end

# ╔═╡ ae32cb91-9713-421d-a651-1235543672ed
begin
	struct NormalFormGame{N,T<:Real}
	    players::NTuple{N,Player{N,T}}
	    nums_actions::NTuple{N,Int}
	end
	
	num_players(::NormalFormGame{N}) where {N} = N
	
	function NormalFormGame(::Tuple{})  # To resolve definition ambiguity
	    throw(ArgumentError("input tuple must not be empty"))
	end
	
	function NormalFormGame(T::Type, nums_actions::NTuple{N,Int}) where N
    players = Vector{Player{N,T}}(undef, N)
    for i in 1:N
        sz = ntuple(j -> nums_actions[i-1+j <= N ? i-1+j : i-1+j-N], N)
        players[i] = Player(zeros(T, sz))
    end
    return NormalFormGame(players)
    end
	
	NormalFormGame(nums_actions::NTuple{N,Int}) where {N} =
    NormalFormGame(Float64, nums_actions)
	
	# Constructor of an N-player NormalFormGame with a tuple of N Player instances.
	function NormalFormGame(players::NTuple{N,Player{N,T}}) where {N,T}
    is_consistent(players) ||
        throw(ArgumentError("shapes of payoff arrays must be consistent"))
    nums_actions = ntuple(i -> num_actions(players[i]), N)
    return NormalFormGame{N,T}(players, nums_actions)
    end
	
	#Constructor of an N-player NormalFormGame with a vector of N Player instances.
    NormalFormGame(players::Vector{Player{N,T}}) where {N,T} =
    NormalFormGame(ntuple(i -> players[i], N))
	
	# Constructor of an N-player NormalFormGame with N Player instances.
	function NormalFormGame(players::Player{N,T}...) where {N,T}
    length(players) != N && error("Need $N players")
    NormalFormGame(players)  # use constructor for Tuple of players above
    end
	
	#NormalFormGame(payoffs)Construct an N-player NormalFormGame for N>=2 with an   array `payoffs` of M=N+1 dimensions, where `payoffs[a_1, a_2, ..., a_N, :]` contains a profile of N payoff values
	function NormalFormGame(payoffs::Array{T,M}) where {T<:Real,M}
    N = M - 1
    dims = Base.front(size(payoffs))
    colons = Base.front(ntuple(j -> Colon(), M))

    size(payoffs)[end] != N && throw(ArgumentError(
        "length of the array in the last axis must be equal to
         the number of players"
    ))

    players = ntuple(
        i -> Player(permutedims(view(payoffs, colons..., i),
                                     (i:N..., 1:i-1...)::typeof(dims))
                   ),
        Val(N)
    )
    NormalFormGame(players)
    end
	
	# NormalFormGame(payoffs) Construct a symmetric 2-player NormalFormGame with a square matrix.
	function NormalFormGame(payoffs::Matrix{T}) where T<:Real
    n, m = size(payoffs)
    n != m && throw(ArgumentError(
        "symmetric two-player game must be represented by a square matrix"
    ))
    player = Player(payoffs)
    return NormalFormGame(player, player)
    end
	
	function NormalFormGame{N,T}(g::NormalFormGame{N,S}) where {N,T,S}
    players_new = ntuple(i -> Player{N,T}(g.players[i]), Val(N))
    return NormalFormGame(players_new)
    end

    NormalFormGame(::Type{T}, g::NormalFormGame{N}) where {T<:Real,N} =
    NormalFormGame{N,T}(g)

    NormalFormGame(g::NormalFormGame{N,T}) where {N,T} = NormalFormGame{N,T}(g)


end

# ╔═╡ 16ef8576-455f-4d23-97fd-9166e821e639
begin
	function delete_action(player::Player{N,T}, action::AbstractVector{<:PureAction},
                       player_idx::Integer=1) where {N,T}
    sel = Any[Colon() for i in 1:N]
    sel[player_idx] = setdiff(axes(player.payoff_array, player_idx), action)
    payoff_array_new = player.payoff_array[sel...]::Array{T,N}
    return Player(payoff_array_new)
    end

delete_action(player::Player, action::PureAction, player_idx::Integer=1) =
    delete_action(player, [action], player_idx) 
	
	# delete_action """ delete_action(g, action, player_idx) Return a new  `NormalFormGame` instance with the action(s) specified by `action` deleted from the action set of the player specified by `player_idx`.
	# delete action for pure action
	function delete_action(g::NormalFormGame{N},
                       action::AbstractVector{<:PureAction},
                       player_idx::Integer) where N
    players_new  = [delete_action(player, action,
                    player_idx-i+1>0 ? player_idx-i+1 : player_idx-i+1+N)
                    for (i, player) in enumerate(g.players)]
    return NormalFormGame(players_new)
    end

    delete_action(g::NormalFormGame, action::PureAction, player_idx::Integer) =
    delete_action(g, [action], player_idx)
end

# ╔═╡ ce9d58cb-34b7-4110-8224-b6b078f6c1e9
#  NormalFormGame(T, g) Convert `g` into a new `NormalFormGame` instance with eltype `T`.
Base.convert(::Type{T}, g::NormalFormGame) where {T<:NormalFormGame} =
    g isa T ? g : T(g)

# ╔═╡ eff077ee-6c18-4a8d-84a2-42f98231c8b3
Base.summary(g::NormalFormGame) =
    string(Base.dims2string(g.nums_actions),
           " ",
           split(string(typeof(g)), ".")[end])

# ╔═╡ 4e7bc9ba-f5b6-430f-a3ac-969d0865cfe5
md"#### Function to Display Payoff Arrays"

# ╔═╡ 450295b0-a915-4a70-a3c3-a84eec95459b
# TODO: add printout of payoff arrays
function Base.show(io::IO, g::NormalFormGame)
    print(io, summary(g))
end

# ╔═╡ e06bc295-5e84-4f4a-a7ab-ddce36ff3208
md"##### Player Action value index definition"

# ╔═╡ 2739da27-e60d-4d6b-8f1c-4365d8c599a5
begin
	function Base.getindex(g::NormalFormGame{N,T},
	                       index::Integer...) where {N,T}
	    length(index) != N &&
	        throw(DimensionMismatch("index must be of length $N"))
	
	    payoff_profile = Array{T}(undef, N)
	    for (i, player) in enumerate(g.players)
	        payoff_profile[i] =
	            player.payoff_array[(index[i:end]..., index[1:i-1]...)...]
	    end
	    return payoff_profile
	end
	
	function Base.getindex(g::NormalFormGame{1,T}, index::Integer) where T
	    return g.players[1].payoff_array[index]
	end
	
	# Indexing with CartesianIndices
	Base.getindex(g::NormalFormGame{N}, ci::CartesianIndex{N}) where {N} =
	    g[to_indices(g, (ci,))...]
end

# ╔═╡ 299b6dc2-0386-4319-99f5-2fe879e5dc53
begin
	function Base.setindex!(g::NormalFormGame{N,T},
	                        payoff_profile::Vector{S},
	                        index::Integer...) where {N,T,S<:Real}
	    length(index) != N &&
	        throw(DimensionMismatch("index must be of length $N"))
	    length(payoff_profile) != N &&
	        throw(DimensionMismatch("assignment must be of $N-array"))
	
	    for (i, player) in enumerate(g.players)
	        player.payoff_array[(index[i:end]...,
	                             index[1:i-1]...)...] = payoff_profile[i]
	    end
	    return payoff_profile
	end
	
	# Trivial game with 1 player
	function Base.setindex!(g::NormalFormGame{1,T},
	                        payoff::S,
	                        index::Integer) where {T,S<:Real}
	    g.players[1].payoff_array[index] = payoff
	    return payoff
	end
	
	Base.setindex!(g::NormalFormGame{N}, v, ci::CartesianIndex{N}) where {N} =
	    g[to_indices(g, (ci,))...] = v
	
end

# ╔═╡ 318f514c-725b-467a-9369-dc18d544c08a
md"#### Nash Equilibrium operator"

# ╔═╡ ee7f53f5-3bf3-4db4-b254-104fef382302
begin
	# is_nash
	
	function is_nash(g::NormalFormGame, action_profile::ActionProfile;
	                 tol::Real=1e-8)
	    for (i, player) in enumerate(g.players)
	        own_action = action_profile[i]
	        opponents_actions =
	            tuple(action_profile[i+1:end]..., action_profile[1:i-1]...)
	        if !(is_best_response(player, own_action, opponents_actions, tol=tol))
	            return false
	        end
	    end
	    return true
	end
	
	
	function is_nash(g::NormalFormGame{2}, action_profile::ActionProfile;
	                 tol::Real=1e-8)
	    for (i, player) in enumerate(g.players)
	        own_action, opponent_action =
	            action_profile[i], action_profile[3-i]
	        if !(is_best_response(player, own_action, opponent_action, tol=tol))
	            return false
	        end
	    end
	    return true
	end
	
	# Return true if `action_profile` is a Nash equilibrium
	
	is_nash(g::NormalFormGame{1}, action::Action; tol::Real=1e-8) =
	    is_best_response(g.players[1], action, nothing, tol=tol)
	
	is_nash(g::NormalFormGame{1}, action_profile::ActionProfile;
	        tol::Real=1e-8) = is_nash(g, action_profile..., tol=tol)
	
	# Utility functions
end

# ╔═╡ 97788be5-363a-4375-ac03-a914ecde1009
#Convert a pure action to the corresponding mixed action.
function pure2mixed(num_actions::Integer, action::PureAction)
    mixed_action = zeros(num_actions)
    mixed_action[action] = 1
    return mixed_action
end

# ╔═╡ 591f2e22-106a-4ed0-9778-a6e5733735b5
md"#### Dominant and Weak Strategy type definitions"

# ╔═╡ 330ab06d-f939-4a9a-be6e-8baa915b0d14
begin
	abstract type AbstractOptimizer end
	abstract type Optimizer end
end

# ╔═╡ 19da69c3-180e-4616-9235-fecdf89b35eb
begin
	function is_dominated(
	    ::Type{T}, player::Player{1}, action::PureAction; tol::Real=1e-8,
	    lp_solver::Union{Type{TO},Function}=() -> Optimizer(LogLevel=0)
	) where {T<:Real,TO<:AbstractOptimizer}
	        payoff_array = player.payoff_array
	        return maximum(payoff_array) > payoff_array[action] + tol
	end
	
	is_dominated(
	    player::Player, action::PureAction; tol::Real=1e-8,
	    lp_solver::Union{Type{TO},Function}=() -> Optimizer(LogLevel=0)
	) where {TO<:AbstractOptimizer} =
	    is_dominated(Float64, player, action, tol=tol, lp_solver=lp_solver)
	
end

# ╔═╡ 0f869b2a-1b98-46e0-9743-0e4f00b4934d
begin
	# Return a vector of actions that are strictly dominated by some mixed actions.
	function dominated_actions(
	    ::Type{T}, player::Player; tol::Real=1e-8,
	    lp_solver::Union{Type{TO},Function}=() -> Optimizer(LogLevel=0)
	) where {T<:Real,TO<:AbstractOptimizer}
	    out = Int[]
	    for action = 1:num_actions(player)
	        if is_dominated(T, player, action, tol=tol, lp_solver=lp_solver)
	            append!(out, action);
	        end
	    end
	
	    return out
	end
	
	dominated_actions(
	    player::Player; tol::Real=1e-8,
	    lp_solver::Union{Type{TO},Function}=() -> Optimizer(LogLevel=0)
	) where {TO<:AbstractOptimizer} =
	    dominated_actions(Float64, player, tol=tol, lp_solver=lp_solver)
	
end

# ╔═╡ cee77c24-8c73-4e7c-aeed-94738b1edaaf
md"### Pure Nash"

# ╔═╡ 263cbf74-6bef-485f-817e-8034081c8025
# Finds all pure action Nash equilibria for a normal form game. It returns an empty array if there is no pure action Nash. Uses a brute force algorithm, but that hopefully will change in the future.
function pure_nash(nfg::NormalFormGame; ntofind=prod(nfg.nums_actions),
                   tol::Real=1e-8)
    # Get number of players and their actions
    np = num_players(nfg)
    na = nfg.nums_actions

    # Holder for all NE
    ne = Array{PureActionProfile{np,Int}}(undef, 0)

    # Create counter for how many to find
    nfound = 0

    for _a in CartesianIndices(na)
        if is_nash(nfg, _a.I, tol=tol)
            push!(ne, _a.I)
            nfound = nfound + 1
        end
        nfound >= ntofind && break
    end

    return ne
end

# ╔═╡ 874ebfc5-3a2a-43e8-97ca-33faaba93f39
md"## End of Assignment Dependencies"

# ╔═╡ fd52dd5b-1507-49f8-b0e4-d4c9641cda56
md"# Game Time!!!"

# ╔═╡ 42cf62da-f9cb-4027-a683-64170dc27f5b
md"## First up let's create a 'normal form game' Coin Toss"

# ╔═╡ 76653074-857c-411e-b93c-4df93d8b1dd6
coin_toss_bimatrix = Array{Float64}(undef, 2, 2, 2)

# ╔═╡ 4424ac41-4491-4f7e-8b54-4aea8996d6e4
coin_toss_bimatrix[1, 1, :] = [1, -1]  # payoff profile for action profile (1, 1)

# ╔═╡ ce9440fa-0dfa-495f-84cc-81524ea5f060
coin_toss_bimatrix[1, 2, :] = [-1, 1]

# ╔═╡ 70b131e6-5977-408d-8656-4d0173322223
coin_toss_bimatrix[2, 1, :] = [-1, 1]

# ╔═╡ b57cca0f-23f6-4d84-9d66-b083007934ff
coin_toss_bimatrix[2, 2, :] = [1, -1]

# ╔═╡ 90cc2eb9-5ec6-4e5e-8a2f-4c9a3827a100
g_CoinToss = NormalFormGame(coin_toss_bimatrix)

# ╔═╡ beda2d95-cae3-4561-bcaa-bf6bf3b7f59e
with_terminal() do 
	println(g_CoinToss.players[1])  # Player instance for player 1
end

# ╔═╡ 19826add-873d-4351-ad56-438726310031
with_terminal() do 
	println(g_CoinToss.players[2])  # Player instance for player 2
end

# ╔═╡ b528a306-af68-47f2-a2a1-8251012e3e3a
md"### Player's Payoff Matrix"

# ╔═╡ 0122ce93-6ce8-4a90-b78a-80e19fdbab7a
with_terminal() do 
	println(g_CoinToss.players[1].payoff_array)
end

# ╔═╡ 3718fe33-7360-49ca-942b-70ffd88f2e21
g_CoinToss.players[2].payoff_array

# ╔═╡ 5d871b83-07f8-4db8-8306-670ae4e882ad
g_CoinToss[1, 1]  # payoff profile for action profile (1, 1)

# ╔═╡ 02fbc089-55f3-424b-a158-bfe526ba56de
md"### Next up, a game of Variant Watch"

# ╔═╡ 041c3d6d-157c-4b75-85ac-1ceeee4ef189
variant_watch_matrix = [4 0;
                            3 2]  # square matrix

# ╔═╡ fe753ad8-18e8-492a-a3a3-e177402ce716
g_TVA = NormalFormGame(variant_watch_matrix)

# ╔═╡ 199f5662-6ebc-45b8-b4f2-f07fb7debeb1
with_terminal() do 
	show(g_TVA.players[2].payoff_array)  # Player 2's payoff array
end

# ╔═╡ a5f45434-27d3-4d19-90aa-999be6eba926
md"### Rock, Paper, Scissors"

# ╔═╡ 139b55af-321c-402e-ab54-44a6592eca08
RockPapSza_matrix = [0 -1 1;
              1 0 -1;
              -1 1 0]

# ╔═╡ cee64105-fb25-4457-9037-313ef78f3958
g_RockPapzaS = NormalFormGame(RockPapSza_matrix)

# ╔═╡ ce01c6c7-05ab-4674-85ba-8e38c6997b48
md"### Prison Dilemma Game"

# ╔═╡ 97498e58-8cf8-4381-bd5c-5c95bb1285e5
# Game is initiated by specifying the sizes of action sets of the players filled with zero payoff which can be set  to each entry
game_PrisonDilemma = NormalFormGame((2, 2))  # There are 2 players, each of whom has 2 actions

# ╔═╡ 6dc73a65-f5de-4a31-8af7-8085296f42c2
game_PrisonDilemma[1, 1] = [1, 1]

# ╔═╡ 655c6b3f-392b-4848-9189-c3323968ca5f
game_PrisonDilemma[1, 2] = [-2, 3]

# ╔═╡ 2351d0c9-cfdb-48a7-babd-1b05379d183b
game_PrisonDilemma[2, 1] = [3, -2]

# ╔═╡ 6780ae26-b8ea-479d-9455-7645ddcb33bf
game_PrisonDilemma[2, 2] = [0, 0];

# ╔═╡ cd8f68b9-682d-4838-8f39-28171b6280b5
game_PrisonDilemma

# ╔═╡ 7c7ee52a-ceb8-4d78-99d3-81a039659c40
with_terminal() do 
	println(game_PrisonDilemma.players[1].payoff_array)
end

# ╔═╡ d6e9d6d1-379d-41cc-80de-ad8c733dd8b8
#println(playerF.payoff_array)

# ╔═╡ e09e9b41-1aff-4143-bdc1-d42cf5b915c6
md"Multiple Player Game"

# ╔═╡ 220c6e8e-f55d-4334-960a-c59b841627fd
function firm(a::Real, c::Real, ::Val{N}, q_grid::AbstractVector{T}) where {N,T<:Real}
    nums_actions = ntuple(x->length(q_grid), Val(N))
    S = promote_type(typeof(a), typeof(c), T)
    payoff_array= Array{S}(undef, nums_actions)
    for I in CartesianIndices(nums_actions)
        Q = zero(S)
        for i in 1:N
            Q += q_grid[I[i]]
        end
        payoff_array[I] = (a - c - Q) * q_grid[I[1]]
    end
    players = ntuple(x->Player(payoff_array), Val(N))
    return NormalFormGame(players)
end

# ╔═╡ 82591727-15fe-4e29-b3fd-40c20f419255
md"## Sequential Best Response Games"

# ╔═╡ 7ca40b90-a64a-4959-98b8-3d2a23440f7b
function sequential_best_response(g::NormalFormGame{N};
                                  init_actions::Vector{Int}=ones(Int, N),
                                  tie_breaking=:smallest,
                                  verbose=true) where N
    a = copy(init_actions)
    if verbose
        println("init_actions: $a")
    end
    
    new_a = Array{Int}(undef, N)
    max_iter = prod(g.nums_actions)
    
    for t in 1:max_iter
        copyto!(new_a, a)
        for (i, player) in enumerate(g.players)
            if N == 2
                a_except_i = new_a[3-i]
            else
                a_except_i = (new_a[i+1:N]..., new_a[1:i-1]...)
            end
            new_a[i] = best_response(player, a_except_i,
                                     tie_breaking=tie_breaking)
            if verbose
                println("player $i: $new_a")
            end
        end
        if new_a == a
            return a
        else
            copyto!(a, new_a)
        end
    end
    
    println("No pure Nash equilibrium found")
    return a
end

# ╔═╡ b11cf5a1-95e9-42f2-b52c-5eed0fec1f51
a, c = 80, 20

# ╔═╡ 788d0d1d-04a0-4235-beb4-604b534fae1d
N = 3

# ╔═╡ 4ee2039d-6f4f-41f3-b90e-d19f3c0640ce
q_grid_size = 13

# ╔═╡ ad315520-0acc-414c-8ca5-489b94e8c6dd
q_grid = range(0, step=div(a-c, q_grid_size-1), length=q_grid_size)  # [0, 5, 10, ..., 60]

# ╔═╡ 66b2810d-ba64-4e93-9773-cb04084aefa1
game_Firm = firm(a, c, Val(N), q_grid)

# ╔═╡ 4e7f69a4-7c84-464c-9f2c-77099665737e
md"### End of Assignment"

# ╔═╡ Cell order:
# ╠═82767f90-ddc2-11eb-090d-851aa299afbb
# ╠═31af9cd1-5cb6-4ad0-adea-e48e328e8294
# ╠═86075c96-5e3b-4f8b-bc10-b5e2e9a4fc40
# ╠═68fb6208-b9a3-49a7-8ef3-39abadc56cc9
# ╠═53353ddc-c878-4d24-a0e1-f95d98ba8624
# ╠═ea2f9c6f-f7fc-4c27-9aea-6526d7ea215d
# ╠═dce39d06-7b16-45a9-aa49-9e371bcc03f6
# ╠═438cddda-45e1-4b1a-828e-e7780b3c1c6b
# ╠═bac84107-4d68-4b5d-b178-f4cb6c531b3f
# ╠═53fccfd5-9bea-47e4-ac45-f99394e3edd7
# ╠═6f8804a1-d2b8-4c09-80ac-d581ede71cde
# ╠═d4c2bf5b-60cc-4cb9-8143-3b7a333a67c0
# ╠═28632595-ed94-43e7-8b7d-32595eed0e9b
# ╠═3129bff0-f4fc-4d91-8ffe-484404e8289f
# ╠═246fc523-4ea7-4241-ae88-fea763c69395
# ╠═4b6e7fc6-9ba4-460a-9b77-c40d651365eb
# ╠═e094541b-c5a8-4027-b007-e9eb016d0ad8
# ╠═ebea3dec-353a-4281-be79-4b6d4cdffc98
# ╠═5af39e18-3590-439c-9efc-127df06b7819
# ╠═507a3894-7213-4f7d-a568-57a455977c66
# ╠═df4fe028-bc0e-45a8-8018-5679348f319d
# ╠═2809d30d-9229-4551-960a-c30d716f0693
# ╠═318543c1-2e9f-40bd-8364-5ed9e4feeade
# ╠═74c6b565-8cd3-48f0-a197-241d177245c0
# ╠═4ef765ed-7e3b-4588-81f2-b0ced5ca0356
# ╠═cd4df2be-8538-41a0-aad8-71bd9e477f9a
# ╠═7e9c8d2f-2cba-4195-a7fc-4fe776930ccc
# ╠═de06ae24-5f0c-496e-b4fc-84015cbdf926
# ╠═1333118f-e4b6-447f-9962-843a49aa2cea
# ╠═64962491-a1ad-4fac-9097-d5f4a56e1d05
# ╠═a106f03c-2a93-406a-9190-724a18680e94
# ╠═ae32cb91-9713-421d-a651-1235543672ed
# ╠═16ef8576-455f-4d23-97fd-9166e821e639
# ╠═ce9d58cb-34b7-4110-8224-b6b078f6c1e9
# ╠═eff077ee-6c18-4a8d-84a2-42f98231c8b3
# ╠═4e7bc9ba-f5b6-430f-a3ac-969d0865cfe5
# ╠═450295b0-a915-4a70-a3c3-a84eec95459b
# ╠═e06bc295-5e84-4f4a-a7ab-ddce36ff3208
# ╠═2739da27-e60d-4d6b-8f1c-4365d8c599a5
# ╠═299b6dc2-0386-4319-99f5-2fe879e5dc53
# ╠═318f514c-725b-467a-9369-dc18d544c08a
# ╠═ee7f53f5-3bf3-4db4-b254-104fef382302
# ╠═97788be5-363a-4375-ac03-a914ecde1009
# ╠═591f2e22-106a-4ed0-9778-a6e5733735b5
# ╠═330ab06d-f939-4a9a-be6e-8baa915b0d14
# ╠═19da69c3-180e-4616-9235-fecdf89b35eb
# ╠═0f869b2a-1b98-46e0-9743-0e4f00b4934d
# ╠═cee77c24-8c73-4e7c-aeed-94738b1edaaf
# ╠═263cbf74-6bef-485f-817e-8034081c8025
# ╠═874ebfc5-3a2a-43e8-97ca-33faaba93f39
# ╠═fd52dd5b-1507-49f8-b0e4-d4c9641cda56
# ╠═42cf62da-f9cb-4027-a683-64170dc27f5b
# ╠═76653074-857c-411e-b93c-4df93d8b1dd6
# ╠═4424ac41-4491-4f7e-8b54-4aea8996d6e4
# ╠═ce9440fa-0dfa-495f-84cc-81524ea5f060
# ╠═70b131e6-5977-408d-8656-4d0173322223
# ╠═b57cca0f-23f6-4d84-9d66-b083007934ff
# ╠═90cc2eb9-5ec6-4e5e-8a2f-4c9a3827a100
# ╠═ee3a001d-d7d9-45f4-897a-bc55aedac55a
# ╠═beda2d95-cae3-4561-bcaa-bf6bf3b7f59e
# ╠═19826add-873d-4351-ad56-438726310031
# ╠═b528a306-af68-47f2-a2a1-8251012e3e3a
# ╠═0122ce93-6ce8-4a90-b78a-80e19fdbab7a
# ╠═3718fe33-7360-49ca-942b-70ffd88f2e21
# ╠═5d871b83-07f8-4db8-8306-670ae4e882ad
# ╠═02fbc089-55f3-424b-a158-bfe526ba56de
# ╠═041c3d6d-157c-4b75-85ac-1ceeee4ef189
# ╠═fe753ad8-18e8-492a-a3a3-e177402ce716
# ╠═199f5662-6ebc-45b8-b4f2-f07fb7debeb1
# ╠═a5f45434-27d3-4d19-90aa-999be6eba926
# ╠═139b55af-321c-402e-ab54-44a6592eca08
# ╠═cee64105-fb25-4457-9037-313ef78f3958
# ╠═ce01c6c7-05ab-4674-85ba-8e38c6997b48
# ╠═97498e58-8cf8-4381-bd5c-5c95bb1285e5
# ╠═6dc73a65-f5de-4a31-8af7-8085296f42c2
# ╠═655c6b3f-392b-4848-9189-c3323968ca5f
# ╠═2351d0c9-cfdb-48a7-babd-1b05379d183b
# ╠═6780ae26-b8ea-479d-9455-7645ddcb33bf
# ╠═cd8f68b9-682d-4838-8f39-28171b6280b5
# ╠═7c7ee52a-ceb8-4d78-99d3-81a039659c40
# ╠═d6e9d6d1-379d-41cc-80de-ad8c733dd8b8
# ╠═e09e9b41-1aff-4143-bdc1-d42cf5b915c6
# ╠═220c6e8e-f55d-4334-960a-c59b841627fd
# ╠═82591727-15fe-4e29-b3fd-40c20f419255
# ╠═7ca40b90-a64a-4959-98b8-3d2a23440f7b
# ╠═b11cf5a1-95e9-42f2-b52c-5eed0fec1f51
# ╠═788d0d1d-04a0-4235-beb4-604b534fae1d
# ╠═4ee2039d-6f4f-41f3-b90e-d19f3c0640ce
# ╠═ad315520-0acc-414c-8ca5-489b94e8c6dd
# ╠═66b2810d-ba64-4e93-9773-cb04084aefa1
# ╠═4e7f69a4-7c84-464c-9f2c-77099665737e
