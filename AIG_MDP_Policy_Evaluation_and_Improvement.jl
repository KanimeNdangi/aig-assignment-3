### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 0f95e984-7ad9-429c-8dff-c12b8bfa475d
using PlutoUI

# ╔═╡ 98f7fd6f-a928-48a6-a6e3-7b6dde1026f1
md"## Julia program that captures and stores mdp at runtime and given policy, evaluate policy and improve to get a solution"

# ╔═╡ 9a2fefd4-3e67-4092-9ebc-7048c9043554
md"### Abstract MDP type definition"

# ╔═╡ 1ca0491a-881d-48da-ac65-04d18ee499d9
abstract type MarkoDecisionProcess end

# ╔═╡ 5190ca8f-c8aa-45b9-8417-e5f902b646e2
md"### State definition"

# ╔═╡ 01b52cb6-6bb0-4ff8-bdc5-19cba4768f1b
begin
	struct MDPState 
	    x::Int64 # horizontal position
	    y::Int64 # vertical position
	    done::Bool # check to see if in a terminal state?
	end
	
	MDPState(x::Int64, y::Int64) = MDPState(x,y,false) # Initial state contstructor
	posequal(s1::MDPState, s2::MDPState) = s1.x == s2.x && s1.y == s2.y # position check
end

# ╔═╡ 3b70b842-c4e7-4c0f-b150-a7ebd07070f1
 md"### Action definitions"

# ╔═╡ 6fa5fcbe-a483-43d3-a65e-0ce55c3e7fb8
struct Action
	direction::String
	probability::Float64
end

# ╔═╡ fdafaba8-31aa-40c5-876f-d6635340578d
# Assign directions for agent

# ╔═╡ fbc8ac66-5dd4-4ffe-a77d-8278c6b47114
Dir1 = Action("up", 0.8)

# ╔═╡ c3a393cc-f93a-4e65-be81-f1241a69e48b
Dir2 = Action("left", 0.1)

# ╔═╡ 5adc7fdf-631c-4ca7-9f0d-3e963182a4cd
Dir3 = Action("right",0.1)

# ╔═╡ e83ec5d9-c088-41e1-95e0-b50ce7dd5ca0
md"### State world definition to hold information for MDP tuple (State, Action, TransitionProbability, Reward)"

# ╔═╡ b920ddd8-98f6-4119-873e-670fd33f158f
abstract type MDP{S,A} end

# ╔═╡ f6abbd45-7bb3-4488-8533-660d393145fa
begin
	mutable struct MDPWorld <: MDP{MDPState, Symbol} # MDP is parametarized by the state and the action
	    size_x::Int64 # horizontal size of the grid
	    size_y::Int64 # vertical size of the grid
	    reward_states::Vector{MDPState} # states of reward
	    reward_values::Vector{Float64} # reward values
	    tprob::Float64 # transitionin probaility to the desired state
	    discount_factor::Float64 # disocunt factor
	
	end
	function MDPWorld(;sx::Int64=4, # size_x
	                    sy::Int64=4, # size_y
	                    rs::Vector{MDPState}=[MDPState(2,1), MDPState(2,3),MDPState(1,2), MDPState(4,3)], # reward states
	                    rv::Vector{Float64}=rv = [-10.,-5,3,10], # reward values
	                    tp::Float64=0.8, # transition probability
	                    discount_factor::Float64=0.9)
	    return MDPWorld(sx, sy, rs, rv, tp, discount_factor)
	end
	
	# we can now create an MDPWorld mdp instance like this:
	mdp = MDPWorld()
	with_terminal() do 
		@show mdp.reward_states # Displays the defualt values from the constructor
	end
end

# ╔═╡ 8c672e63-faeb-4eae-a09b-b3c645a1aa48
md"#### State Space, all the states in the problem over which I'll iterate over"

# ╔═╡ 4831b32e-d70e-4529-a66e-707745afeecc
begin
	abstract type states end
	function states(mdp::MDPWorld)
	    s = MDPState[] # initialize an array of MDPStates
	    # loop over all our states
	    # done (d)
	    for d = 0:1, y = 1:mdp.size_y, x = 1:mdp.size_x
	        push!(s, MDPState(x,y,d))
	    end
	    return s
	end
end

# ╔═╡ ccea2078-ed3e-4604-bb96-7affa972b497
begin
	#mdp = MDPWorld()
	state_space = states(mdp);
	state_space[1]
end

# ╔═╡ aaf26ebb-4f84-4078-89e1-7c62ae5ca080
md"#### The action space is the set of all actions availiable to the agent."

# ╔═╡ 8c775b2d-1dc4-497a-ac44-3bddd8fb0236
begin
	abstract type actions end
	actions(mdp::MDPWorld) = [:up, :left, :right];
end

# ╔═╡ b63fe81b-da76-4ded-9639-63a15365095e
md"### Probability Distribution definitions"

# ╔═╡ 595343a8-b939-489f-ae88-34d496d91c7b
begin
	# transition helpers
	function inbounds(mdp::MDPWorld,x::Int64,y::Int64)
	    if 1 <= x <= mdp.size_x && 1 <= y <= mdp.size_y
	        return true
	    else
	        return false
	    end
	end
	
	inbounds(mdp::MDPWorld, state::MDPState) = inbounds(mdp, state.x, state.y);
end

# ╔═╡ c2e0c096-1ee0-4c4a-aa6d-2e087a886772
md"### Transition Model Definition"

# ╔═╡ 494343b7-9421-4399-80ec-4c58d62596ba
begin
	abstract type transition end
	function transition(mdp::MDPWorld, state::MDPState, action::Symbol)
	    a = action
	    x = state.x
	    y = state.y
	    
	    if state.done
	        return SparseCat([MDPState(x, y, true)], [1.0])
	    elseif state in mdp.reward_states
	        return SparseCat([MDPState(x, y, true)], [1.0])
	    end
	
	    neighbors = [
	        MDPState(x+1, y, false), # right
	        MDPState(x-1, y, false), # left
	        MDPState(x, y+1, false), # up
	        ] # See Performance Note below
	    
	    targets = Dict(:right=>1, :left=>2, :up=>3) # See Performance Note below
	    target = targets[a]
	    
	    probability = fill(0.0, 3)
	
	    if !inbounds(mdp, neighbors[target])
	        # If would transition out of bounds, stay in
	        # same cell with probability 1
	        return SparseCat([MDPState(x, y)], [1.0]) # SparseCat object contains a vector of states and an associated vector of their probabilities
	    else
	        probability[target] = mdp.tprob
	
	        oob_count = sum(!inbounds(mdp, n) for n in neighbors) # number of out of bounds neighbors
	
	        new_probability = (1.0 - mdp.tprob)/(3-oob_count)
	
	        for i = 1:3 # do not include neighbor 5
	            if inbounds(mdp, neighbors[i]) && i != target
	                probability[i] = new_probability
	            end
	        end
	    end
	
	    return SparseCat(neighbors, probability)
	end;
end

# ╔═╡ c66d7b16-543e-4df9-8436-79ec4df77c63
md"### Reward Model, function that returns the reward of being in state"

# ╔═╡ 5a659e94-6ccf-4694-b4cf-b30a4c30ceb9
begin 
	abstract type reward end 
	
	function reward(mdp::MDPWorld, state::MDPState, action::Symbol, statep::MDPState) #deleted action
    if state.done
        return 0.0
    end
    r = 0.0
    n = length(mdp.reward_states)
    for i = 1:n
        if posequal(state, mdp.reward_states[i])
            r += mdp.reward_values[i]
        end
    end
    return r
end;
end

# ╔═╡ af6e00ec-dc45-4ce2-b09e-f5633c6af9fc
md"#### Discount function"

# ╔═╡ fc2cf89c-3ccb-43ed-a159-cbf11dcf29a5
begin 
	abstract type discount end 
	discount(mdp::MDPWorld) = mdp.discount_factor
end

# ╔═╡ 4cfd440b-e179-40fe-97a2-5f8c44de2567
md"#### Index function to map actions to possible states with help from cartesian indices and linear indices"

# ╔═╡ 1d89b962-ff2f-4501-b1d4-608ff3f0cd5d
begin 
	abstract type actionindex end
	abstract type stateindex end
	function stateindex(mdp::MDPWorld, state::MDPState)
	    sd = Int(state.done + 1)
	    ci = CartesianIndices((mdp.size_x, mdp.size_y, 2))
	    return LinearIndices(ci)[state.x, state.y, sd]
	end
	
	function actionindex(mdp::MDPWorld, act::Symbol)
	    if act==:up
	        return 1
	    elseif act==:left
	        return 2
	    elseif act==:right
	        return 3
	    end
	    error("Invalid GridWorld action: $act")
	end
end

# ╔═╡ b30bbcaa-0c68-4dc0-8589-ea98a9b08cfe
md"### Check terminal state"

# ╔═╡ 59fb61e8-221d-4e4e-a4b8-6f09f228a264
begin 
	abstract type isterminal end 
	isterminal(mdp::MDPWorld, s::MDPState) = s.done
end

# ╔═╡ c07b49f5-ff18-449a-975e-ca2a468b34ac
begin 
abstract type initialstate end
initialstate(pomdp::MDPWorld) = Deterministic(MDPState(1,1))
end

# ╔═╡ c807228f-babe-46ba-a7e4-d687bac40ec5
#mdp = GridWorld()
mdp.tprob=1.0

# ╔═╡ f3ff4b05-c686-4718-a051-a4f04bec7f2c
md"##### MDP Testing/ Simuation with stepthrough function to help me loop and explore the mdp's behaviour"

# ╔═╡ 065ec9c2-2f1c-4efd-bfb4-39690cb744a6
using 

# ╔═╡ 93122f76-e030-427e-aeff-ea361e5967fe
#=begin
	using POMDPPolicies, POMDPSimulators
	begin	
		with_terminal() do 
			for (s,a,r) in stepthrough(mdp, right_policy, "s,a,r", max_steps=10)
		    @show s
		    @show a
		    @show r
		    println()
		end
		end
	en=
end=#


# ╔═╡ f43b7ab5-40e4-43b2-a68c-01de8858c844
md"#### Value Iteration Policy Solver"

# ╔═╡ 54331daa-0602-4031-96bd-9d3483678a73
begin
	function valueIteration(path::MDPWorld, epsilon::Float64=0.001) where T<:AbstractFloat
	
		local U_prime::Dict = Dict(collect(state) for state in path.states);
		
		while(true)
			local U::Dict = copy(U_prime);
			local delta::Float64 = 0.0;
			
			for state in path.states
				U_prime[state] = (path.rewards(path.states) + (path.discount_factor
						* max((sum(collect(p * U[state_prime]
									for(p, state_prime) in addMDPGridTransition!))
								for action in path.actions)...)));
				delta = max(delta, abs(U_prime[state] - U[state]));
			end
			
			if(delta < ((epsilon * (1 - path.discount_factor))/path.discount_factor))
				return U;
			end
		end
			
	end
end

# ╔═╡ d4b043d4-22cd-4522-947c-5cfd662b5a5e
md"#### Evaluation"

# ╔═╡ 15bb9fec-a98c-4aa1-bc82-1f7a47c6f2c2
begin
	function policyEvaluation(pi::Dict, U::Dict, tree::MDPWorld; k::Int64 = 20) where T<:AbstractFloat
		
		for i in 1:k
			for state in tree.states
				U[state] = (rewards(tree, state) + (tree.gamma * sum((p * U[state_prime]
						for(p, state_prime) in addMDPGridTransition!(tree, state, pi[state])))));
			end
		end
	
		return U;
			
	end
	
	function policy(Graph::MDPWorld) where T<:AbstractFloat
		
		local U::Dict = Dict(collect(state) for state in graph.states);
		
		local pi::Dict = Dict(collect(state, rand(RandomDeviceInstance, collect(action(graph, state))))
		for state in graph.states);
		
		while(true)
			U = policyEvaluation(pi, U, graph);
			
			local unchanged::Bool = true;
			
			for state in graph.states
				local action = argmax(collect(actions(graph, state)),
				(function(action::String)
						return expectedUtility(graph, U, state, action);
						end));
				
				if(action != pi[state])
					pi[state] = action;
					unchanged = false;
				end
			end
			
			if(unchanged)
				return pi;
			end
		end
	end
end

# ╔═╡ b720172d-700a-49e9-a5ab-5b97172fd97d


# ╔═╡ 18b1225f-0710-4ca6-877f-de9f013d0865
function MDPdisplay(cube::MDPWorld)
	
    number_of_iterations = cube.number_of_iterations
	
    println("MDP Printing: (Iteration = $number_of_iterations)")
	
    for state in cube.sy
        if state == cube.rs
            continue
        end
		
        print("State = $state")
		
        val = cube.rv[state]
		
        action = cube.tp[state]
		
        if (state, tp.rs[1], graph.tp) in cube.transitions
			print(" (end)")
        end
		
        println("\n\tValue  = $val")
        println("\tAction = $action")
    end
	
    println("Markov Decision Process Display End")
end

# ╔═╡ d82f20d4-8510-4185-a332-fa3f0b20f409
function GridCube(	
   ;cube_nr::Int64 = 4, # size_x
    box_nr::Int64 = 4, # size_y
	reward_s::Vector{MDPState}=[MDPState(1,1), MDPState(1,2),MDPState(2,1), MDPState(4,3)], # reward states
	rewardv::Vector{Float64}=rv = [1.,-5,3,10], # reward values
	transp::Float64=0.8, # transition probability
	disc_factor::Float64=0.9)
	return GridCube(cube_nr, box_nr, reward_s, rewardv, transp, disc_factor)
end

# ╔═╡ d4508aca-4202-4d01-b3c0-fee0a348310b
# say we are in state (4,2)
s = MDPState(4,2)

# ╔═╡ 187cf2d4-ed3b-4861-9f85-d83ce9a4bade
s1 = MDPState(3,3)

# ╔═╡ c837eb34-1e9a-442e-b0ab-159356ea8a84
md"### End of Assignment"

# ╔═╡ Cell order:
# ╠═98f7fd6f-a928-48a6-a6e3-7b6dde1026f1
# ╠═9a2fefd4-3e67-4092-9ebc-7048c9043554
# ╠═1ca0491a-881d-48da-ac65-04d18ee499d9
# ╠═5190ca8f-c8aa-45b9-8417-e5f902b646e2
# ╠═01b52cb6-6bb0-4ff8-bdc5-19cba4768f1b
# ╠═3b70b842-c4e7-4c0f-b150-a7ebd07070f1
# ╠═6fa5fcbe-a483-43d3-a65e-0ce55c3e7fb8
# ╠═fdafaba8-31aa-40c5-876f-d6635340578d
# ╠═fbc8ac66-5dd4-4ffe-a77d-8278c6b47114
# ╠═c3a393cc-f93a-4e65-be81-f1241a69e48b
# ╠═5adc7fdf-631c-4ca7-9f0d-3e963182a4cd
# ╟─e83ec5d9-c088-41e1-95e0-b50ce7dd5ca0
# ╠═b920ddd8-98f6-4119-873e-670fd33f158f
# ╠═f6abbd45-7bb3-4488-8533-660d393145fa
# ╟─8c672e63-faeb-4eae-a09b-b3c645a1aa48
# ╠═4831b32e-d70e-4529-a66e-707745afeecc
# ╠═ccea2078-ed3e-4604-bb96-7affa972b497
# ╟─aaf26ebb-4f84-4078-89e1-7c62ae5ca080
# ╠═8c775b2d-1dc4-497a-ac44-3bddd8fb0236
# ╠═b63fe81b-da76-4ded-9639-63a15365095e
# ╠═595343a8-b939-489f-ae88-34d496d91c7b
# ╠═c2e0c096-1ee0-4c4a-aa6d-2e087a886772
# ╠═494343b7-9421-4399-80ec-4c58d62596ba
# ╟─c66d7b16-543e-4df9-8436-79ec4df77c63
# ╠═5a659e94-6ccf-4694-b4cf-b30a4c30ceb9
# ╠═af6e00ec-dc45-4ce2-b09e-f5633c6af9fc
# ╠═fc2cf89c-3ccb-43ed-a159-cbf11dcf29a5
# ╟─4cfd440b-e179-40fe-97a2-5f8c44de2567
# ╠═1d89b962-ff2f-4501-b1d4-608ff3f0cd5d
# ╠═b30bbcaa-0c68-4dc0-8589-ea98a9b08cfe
# ╠═59fb61e8-221d-4e4e-a4b8-6f09f228a264
# ╠═c07b49f5-ff18-449a-975e-ca2a468b34ac
# ╠═c807228f-babe-46ba-a7e4-d687bac40ec5
# ╠═0f95e984-7ad9-429c-8dff-c12b8bfa475d
# ╟─f3ff4b05-c686-4718-a051-a4f04bec7f2c
# ╠═065ec9c2-2f1c-4efd-bfb4-39690cb744a6
# ╠═93122f76-e030-427e-aeff-ea361e5967fe
# ╠═f43b7ab5-40e4-43b2-a68c-01de8858c844
# ╠═54331daa-0602-4031-96bd-9d3483678a73
# ╠═d4b043d4-22cd-4522-947c-5cfd662b5a5e
# ╠═15bb9fec-a98c-4aa1-bc82-1f7a47c6f2c2
# ╠═b720172d-700a-49e9-a5ab-5b97172fd97d
# ╠═18b1225f-0710-4ca6-877f-de9f013d0865
# ╠═d82f20d4-8510-4185-a332-fa3f0b20f409
# ╠═d4508aca-4202-4d01-b3c0-fee0a348310b
# ╠═187cf2d4-ed3b-4861-9f85-d83ce9a4bade
# ╠═c837eb34-1e9a-442e-b0ab-159356ea8a84
